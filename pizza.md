### API et représentation des données

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas             | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (I3)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza (I3) ou 404                                            |
| /pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizzas/{id}/ingredients   | GET         | <-text/plain                                                 |                 | les ingredients de la pizza ou 404                                        |
| /pizzas             | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Nom (I1)<br>Ingrédient (In) | Nouvel pizza (I3)<br>409 si la pizza existe déjà (même nom) |
| /pizzas/{id}        | DELETE      |                                                              |                 |                                                                      |

<br>
Une pizza comporte uniquement un identifiant, un nom et des ingredients. Sa
représentation JSON (I3) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "margherita",
      "ingredients": 
            [{"id":"c77deeee-d50d-49d5-9695-c98ec811f762","name":"tomate"},
             {"id":"657f8dd4-6bc1-4622-9af7-37d248846a23","name":"fromage"}]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente un ingrédient. Aussi on aura une
représentation JSON (I2) qui comporte uniquement le nom et les ingredients:

    { "name": "margherita", "ingredients": [{"id":"c77deeee-d50d-49d5-9695-c98ec811f762","name":"tomate"},{"id":"657f8dd4-6bc1-4622-9af7-37d248846a23","name":"fromage"}] }

