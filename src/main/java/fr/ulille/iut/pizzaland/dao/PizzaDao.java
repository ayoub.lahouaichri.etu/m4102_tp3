package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizza (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL); "
    		+ "CREATE TABLE IF NOT EXISTS ingredientsPizza "
    		+ "( idIngredients VARCHAR(128), "
    		+ "idPizzas VARCHAR(128), "
    		+ "FOREIGN KEY(idIngredients) REFERENCES ingredients(id), "
    		+ "FOREIGN KEY(idPizzas) REFERENCES pizzas(id), "
    		+ "PRIMARY KEY (idPizzas,idIngredients))  ")
    
    void createTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizza")
    void dropTable();

    @SqlUpdate("INSERT INTO pizza (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);

    @SqlUpdate("DELETE FROM pizza WHERE id = :id")
    void remove(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM pizza WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM Pizza")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM pizza WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
    
    @SqlUpdate("INSERT INTO ingredientsPizza (idIngredients, idPizzas) VALUES (:ingredient, :pizza.id) ")
    void insertWithIngredient(@BindBean UUID pizza, UUID ingredient);
    
    @Transaction
	default void createTablePizzaAndIngredientAssociation() {
		createTable();
	}
	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropTableIngredientsPizza();
		dropTable();
	}

	void dropTableIngredientsPizza();

	@Transaction
	default void deletePizzasByID(UUID id) {
		removeIngredientsPizza(id);
		remove(id);
	}

	void removeIngredientsPizza(UUID id);

	@Transaction
	default void insertIntoPizzas(Pizza pizza) {
		insert(pizza);
		for (Ingredient ingredient : pizza.getIngredients()) {
			insertWithIngredient(pizza.getId(),ingredient.getId());
		}    	
	}


	@Transaction
	default Pizza getFromId(UUID id) {
		IngredientDao ingredients = BDDFactory.buildDao(IngredientDao.class);
		Pizza p = findById(id);
		List<UUID> idList= findByIdIngredient(id);
		for(UUID idI : idList) {
			p.addOneIngredient(ingredients.findById(idI));
		}
		return p;
	}

	@SqlQuery("SELECT idI FROM ingredientsPizza WHERE idP = :id")
	List<UUID> findByIdIngredient(@Bind("id") UUID id);
    
}