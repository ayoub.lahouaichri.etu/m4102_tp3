package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.UUID;


public class PizzaCreateDto {
	private String name;
	private ArrayList<UUID> ingredients;
		
	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

	public ArrayList<UUID> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<UUID> tmp) {
		this.ingredients = tmp;
	}
	
	
}
